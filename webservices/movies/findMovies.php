<?php
	
	header('Allow-access-allow-origin: *');
	header('Content-type: application/json; charset=utf-8');


	include '../../database.php';
	include '../objects/movies.php';

    $title = isset($_GET['title']) ? $_GET['title'] : '';

	$database = new Database();
	$db = $database->getConnection();
	$movie = new Movies($db);
   
	$stmt = $movie->getMovie($title);
	
	if ($stmt->rowCount() > 0){
		echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
	}else{
		echo json_encode(array());
	}

?>